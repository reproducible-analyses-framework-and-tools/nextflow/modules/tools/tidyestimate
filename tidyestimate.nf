process tidyestimate {

  label 'tidyestimate_container'
  label 'tidyestimate'

  input:
  tuple val(pat_name), val(run), val(dataset), path(gene_count_mtx)

  output:
  tuple val(pat_name), val(run), val(dataset), path("*tumor_purity.tsv"), optional: true, emit: tumor_purities
  script:
  """
  echo "library(tidyestimate)" >> CMD
  echo "gene_count_mtx <- read.table(\\"${gene_count_mtx}\\", header=T, sep='\\t')" >> CMD
  echo "filtered <- filter_common_genes(gene_count_mtx, id = 'hgnc_symbol', tidy = FALSE, tell_missing = TRUE, find_alias = TRUE)" >> CMD
  echo "scored <- estimate_score(filtered, is_affymetrix = FALSE)" >> CMD
  echo 'write.table(scored, "tmp", col.names=T, row.names=T, sep="\t", quote=F)' >> CMD
  chmod +x CMD
  Rscript CMD
  mv tmp ${dataset}-${pat_name}-${run}.tidyestimate.tumor_purity.tsv
  """
}
